describe('ventana principal',  () => {
    it('tiene encabezado correcto y en español por defecto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('App con Angular');
        cy.get('h1 b').should('contain', 'HOLA es');
    });
});
describe('Formulario',  () => {
    it('Posee nombre e imagen url', () => {
        cy.visit('http://localhost:4200');
        cy.contains('Whishlist');
        cy.get('label').should('contain', 'Nombre', 'Imagen Url', 'Nombre requerido', 'URL requerido');
    });
});
describe('Acitvidad',  () => {
    it('Registra actividad', () => {
        cy.visit('http://localhost:4200');
        cy.contains('Actividad');
    });
});
describe('Enlace a Login',  () => {
    it('Posee login', () => {
        cy.visit('http://localhost:4200');
        cy.contains('Iniciar sesión');
    });
});