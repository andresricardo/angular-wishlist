import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservasRoutingModule } from './reservas-routing.module';
import { ReservasListadoComponent } from './reservas-listado/reservas-listado.component';
import { ReservasDestalleComponent } from './reservas-destalle/reservas-destalle.component';
import { ReservasApiClientService } from './reservas-api-client.service';


@NgModule({
  declarations: [ReservasListadoComponent, ReservasDestalleComponent],
  providers: [
    ReservasApiClientService
  ],
  imports: [
    CommonModule,
    ReservasRoutingModule
  ]
})
export class ReservasModule { }
